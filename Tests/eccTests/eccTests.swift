import XCTest
@testable import ecc

final class eccTests: XCTestCase {
    let a = AnyFieldElement(number: 7, order: 13)
    let b = AnyFieldElement(number: 6, order: 13)
    let c = AnyFieldElement(number: 7, order: 57)

    func testEquality() {
        XCTAssertTrue(a == a)
        XCTAssertFalse(a == b)
    }

    func testModulo() {
        XCTAssertTrue(-13 %% 12 == 11)
        XCTAssertTrue(7 %% 3 == 1)
        XCTAssertTrue(14738495684013 %% 60 == 33)
    }

    func testAddition() {
        XCTAssertTrue(a + b == AnyFieldElement(number: 0, order: 13))
        XCTAssertTrue(a! + b == AnyFieldElement(number: 0, order: 13))
        XCTAssertTrue(a + b! == AnyFieldElement(number: 0, order: 13))
        XCTAssertTrue(a! + b! == AnyFieldElement(number: 0, order: 13))
        XCTAssertNil(a + c)
    }

    func testSubtraction() {
        XCTAssertTrue(a - b == AnyFieldElement(number: 1, order: 13))
        XCTAssertTrue(a! - b == AnyFieldElement(number: 1, order: 13))
        XCTAssertTrue(a - b! == AnyFieldElement(number: 1, order: 13))
        XCTAssertTrue(a! - b! == AnyFieldElement(number: 1, order: 13))
        XCTAssertNil(a - c)
    }

    func testMultiplication() {
        XCTAssertTrue(a! * b! == AnyFieldElement(number: 3, order: 13))
        XCTAssertTrue(a! * b == AnyFieldElement(number: 3, order: 13))
        XCTAssertTrue(a * b! == AnyFieldElement(number: 3, order: 13))
        XCTAssertTrue(a * b == AnyFieldElement(number: 3, order: 13))
        XCTAssertNil(a * c)
    }

    func testExponentiation() {
        XCTAssertEqual(pow(base: a, exponent: 6), AnyFieldElement(number: 12, order: 13))
        XCTAssertEqual(pow(base: a!, exponent: 6), AnyFieldElement(number: 12, order: 13)!)
        XCTAssertNil(pow(base: nil as AnyFieldElement?, exponent: 6))
    }

    func testDivision() {
        let dividend = AnyFieldElement(number: 2, order: 19)
        let divisor = AnyFieldElement(number: 7, order: 19)
        XCTAssertEqual(dividend! / divisor!, AnyFieldElement(number: 3, order: 19))
        XCTAssertEqual(dividend / divisor!, AnyFieldElement(number: 3, order: 19))
        XCTAssertEqual(dividend! / divisor, AnyFieldElement(number: 3, order: 19))
        XCTAssertEqual(dividend / divisor, AnyFieldElement(number: 3, order: 19))
    }

    static var allTests = [
        ("Equality", testEquality),
    ]
}
