import XCTest

import eccTests

var tests = [XCTestCaseEntry]()
tests += eccTests.allTests()
XCTMain(tests)