/// Operators

/// Modulo

infix operator %%: MultiplicationPrecedence

public func %%<T: BinaryInteger>(lhs: T, rhs: T) -> T {
    return (lhs % rhs + rhs) % rhs
}

/// Addition
public func +<T: FieldElement>(lhs: T, rhs: T) -> T? {
    guard lhs.order == rhs.order else { return nil }
    return T(
      number: (lhs.number + rhs.number) %% lhs.order,
      order: lhs.order
    )
}

public func +<T: FieldElement>(lhs: T?, rhs: T) -> T? {
    guard
      let lhs = lhs,
      lhs.order == rhs.order
    else { return nil }
    return T(
      number: (lhs.number + rhs.number) %% lhs.order,
      order: lhs.order
    )
}

public func +<T: FieldElement>(lhs: T, rhs: T?) -> T? {
    guard
      let rhs = rhs,
      lhs.order == rhs.order
    else { return nil }
    return T(
      number: (lhs.number + rhs.number) %% lhs.order,
      order: lhs.order
    )
}

public func +<T: FieldElement>(lhs: T?, rhs: T?) -> T? {
    guard
      let lhs = lhs,
      let rhs = rhs,
      lhs.order == rhs.order
    else { return nil }
    return T(
      number: (lhs.number + rhs.number) %% lhs.order,
      order: lhs.order
    )
}

// /// Subtraction

public func -<T: FieldElement>(lhs: T, rhs: T) -> T? {
    guard lhs.order == rhs.order else { return nil }
    return T(
      number: (lhs.number - rhs.number) %% lhs.order,
      order: lhs.order
    )
}

public func -<T: FieldElement>(lhs: T?, rhs: T) -> T? {
    guard
      let lhs = lhs,
      lhs.order == rhs.order
    else { return nil }
    return T(
      number: (lhs.number - rhs.number) %% lhs.order,
      order: lhs.order
    )
}

public func -<T: FieldElement>(lhs: T, rhs: T?) -> T? {
    guard
      let rhs = rhs,
      lhs.order == rhs.order
    else { return nil }
    return T(
      number: (lhs.number - rhs.number) %% lhs.order,
      order: lhs.order
    )
}

public func -<T: FieldElement>(lhs: T?, rhs: T?) -> T? {
    guard
      let lhs = lhs,
      let rhs = rhs,
      lhs.order == rhs.order
    else { return nil }
    return T(
      number: (lhs.number - rhs.number) %% lhs.order,
      order: lhs.order
    )
}

/// Multiplication

public func *<T: FieldElement>(lhs: T, rhs: T) -> T? {
    guard lhs.order == rhs.order else { return nil }
    return T(
      number: (lhs.number * rhs.number) %% lhs.order,
      order: lhs.order
    )
}

public func *<T: FieldElement>(lhs: T, rhs: T?) -> T? {
    guard
      let rhs = rhs,
      lhs.order == rhs.order
    else { return nil }
    return T(
      number: (lhs.number * rhs.number) %% lhs.order,
      order: lhs.order
    )
}
public func *<T: FieldElement>(lhs: T?, rhs: T) -> T? {
    guard
      let lhs = lhs,
      lhs.order == rhs.order
    else { return nil }
    return T(
      number: (lhs.number * rhs.number) %% lhs.order,
      order: lhs.order
    )
}

public func *<T: FieldElement>(lhs: T?, rhs: T?) -> T? {
    guard
      let lhs = lhs,
      let rhs = rhs,
      lhs.order == rhs.order
    else { return nil }
    return T(
      number: (lhs.number * rhs.number) %% lhs.order,
      order: lhs.order
    )
}

/// Exponentiation

@inlinable
func pow<T: BinaryInteger>(base: T, exponent: Int, modulo: T) -> T {
    var result: T = 1
    for _ in 0 ..< Int(exponent) {
        result = result * base %% modulo
    }
    return result
}

func pow<A: FieldElement>(base: A, exponent: Int) -> A {
    return A(
      number: pow(base: base.number, exponent: exponent, modulo: base.order),
      order: base.order
    )!
}

func pow<A: FieldElement>(base: A?, exponent: Int) -> A? {
    guard let base = base else { return nil }
    return A(
      number: pow(base: base.number, exponent: exponent, modulo: base.order),
      order: base.order
    )
}

/// Division

// TODO: Division by zero error
// TODO: casting overflow

func /<T: FieldElement>(lhs: T, rhs: T) -> T? {
    guard rhs.number != 0 else { fatalError("Division by zero") }
    guard lhs.order == rhs.order else { return nil }
    return lhs * pow(base: rhs, exponent: Int(rhs.order) - 2)
}

func /<T: FieldElement>(lhs: T?, rhs: T) -> T? {
    guard rhs.number != 0 else { fatalError("Division by zero") }
    guard let lhs = lhs,
          lhs.order == rhs.order
    else { return nil }
    return lhs * pow(base: rhs, exponent: Int(rhs.order) - 2)
}

func /<T: FieldElement>(lhs: T, rhs: T?) -> T? {
    guard let rhs = rhs,
          lhs.order == rhs.order
    else { return nil }
    guard rhs.number != 0 else { fatalError("Division by zero") }
    return lhs * pow(base: rhs, exponent: Int(rhs.order) - 2)
}

func /<T: FieldElement>(lhs: T?, rhs: T?) -> T? {
    guard let rhs = rhs,
          let lhs = lhs,
          lhs.order == rhs.order
    else { return nil }
    guard rhs.number != 0 else { fatalError("Division by zero") }
    return lhs * pow(base: rhs, exponent: Int(rhs.order) - 2)
}

/// Finite Fields

public protocol FieldElement: Comparable {
    var number: UInt { get }
    var order: UInt { get }
    init?(number: UInt, order: UInt)
}

extension FieldElement {
    public static func < (lhs: Self, rhs: Self) -> Bool {
        return lhs.number < rhs.number
    }
}

public struct AnyFieldElement: FieldElement {
    public let number: UInt
    public let order: UInt

    public init?(number: UInt, order: UInt) {
        guard number < order else { return nil }
        self.number = number
        self.order = order
    }
}
